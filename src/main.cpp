#include <Arduino.h>
#include <Wire.h> // must be included here so that Arduino library object file references work
#include <RtcDS3231.h>
#include <SerialCommand.h>

RtcDS3231<TwoWire> Rtc(Wire);
SerialCommand scmd;

/**
 * HPDL-1414 pinout (digits are ordered 3-2-1-0)
 *  1: D6 data input        12: D5 data input
 *  2: D4 data input        11: D3 data input
 *  3: WR write (pull low)  10: D2 data input
 *  4: A1 digit select       9: D1 data input
 *  5: A0 digit select       8: D0 data input
 *  6: 5V                    7: Gnd
 */

/**
 * Pro Micro pinout (consider pin 1 next to the USB port)
 *  1:  TXO/D1    24:  RAW
 *  2:  RXI/D0    23:  Gnd
 *  3:  Gnd       22:  Reset
 *  4:  Gnd       21:  Vcc
 *  5:  D2/SDA    20:  D21/A3
 *  6: ~D3/SCL    19:  D20/A2
 *  7:  D4/A6     18:  D19/A1
 *  8: ~D5        17:  D18/A0
 *  9: ~D6/A7     16:  D15/SCLK
 * 10:  D7        15:  D14/MISO
 * 11:  D8/A8     14:  D16/MOSI
 * 12: ~D9/A9     13: ~D10/A10
 */

/**
 * My wiring, arduino -> hardware
 * D2/D3 -> I2C
 * D4 -> WR0 (right module)
 * D5 -> D6
 * D6 -> D4
 * D7 -> WR1 (left module)
 * D8 -> A1
 * D9 -> A0
 * D10 -> D0
 * D16 -> D1
 * D14 -> D2
 * D15 -> D3
 * D18 -> D5
 * D21 - a button connected to Gnd (so pull up)
 */

const uint8_t dataPins[7]={10, 16, 14, 15, 6, 18, 5};
unsigned long lastMillis = 0;
boolean colons = true;

// declarations
void scmd_setTime();
void scmd_getTime();
void scmd_getTemp();
void scmd_unrecognised(const char*);

void setup() {
  // get pinmodes sorted
  pinMode(4, OUTPUT); // WR0
  pinMode(5, OUTPUT);
  pinMode(6, OUTPUT);
  pinMode(7, OUTPUT); // WR1
  pinMode(8, OUTPUT);
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(14, OUTPUT);
  pinMode(15, OUTPUT);
  pinMode(16, OUTPUT);
  pinMode(18, OUTPUT);
  pinMode(21, INPUT_PULLUP); // button

  // set the write pins high to start

  digitalWrite(4, HIGH);
  digitalWrite(7, HIGH);

  // serial port, I2C, etc
  Serial.begin(9600); // USB serial
  Rtc.Begin();

  scmd.addCommand("set", scmd_setTime);
  scmd.addCommand("temp", scmd_getTemp);
  scmd.setDefaultHandler(scmd_unrecognised);
}

void writeChar(uint8_t position, byte asciiVal) {
  // spread the ascii value across the data pins
  for (int i = 0; i < 7; i++) {
    int outVal = asciiVal & 1;
    if (i > 4) outVal = !outVal; // I don't know why I have to do this, but I do
    asciiVal = asciiVal >> 1;
    digitalWrite(dataPins[i], outVal);
  }

  // select the correct digit
  digitalWrite(9, position & 1);
  position = position >> 1;
  digitalWrite(8, position & 1);
  position = position >> 1;
  // strobe the correct write pin
  uint8_t writePin = position & 1 ? 7 : 4;
  digitalWrite(writePin, LOW);
  delay(1);
  digitalWrite(writePin, HIGH);
}

void writeString(String text) {
  while (text.length() < 8) {
    text.concat(' ');
  }
  text.toUpperCase();
  for (int i = 0; i < 8; i++) {
    // We'll skip over tildes so as to not change things in place
    if (text[i] != '~') {
      writeChar(7 - i, text[i]);
    }
  }
}

void clear() {
  for (int i = 0; i < 8; i++) {
    writeChar(i, ' ');
  }
}

String formatTime(const RtcDateTime& dt)
{
    char timestring[9]; // 8 chars plus null (I guess)

    snprintf_P(timestring, 
            9,
            PSTR("%02u:%02u:%02u"),
            dt.Hour(),
            dt.Minute(),
            dt.Second() );
    return String(timestring);
}

void loop() {
  scmd.readSerial();

  unsigned long currentMillis = millis();
  int diff = currentMillis - lastMillis;
  if (diff >= 1000) {
    // a second has passed
    lastMillis = currentMillis;
    colons = true;
    RtcDateTime now = Rtc.GetDateTime();
    String nowStr = formatTime(now);
    writeString(nowStr);
  } else if (diff >= 500 && colons) {
    colons = false;
    writeChar(2, ' ');
    writeChar(5, ' ');
  }
}

// serial commands
void scmd_unrecognised(const char*) {
  Serial.println("U WOT M8");
}

void scmd_setTime() {
  char *arg = scmd.next();

  if (arg != NULL) {
    String timeStr = String(arg);
    if (timeStr.length() != 6) {
      Serial.println("Provide time as hhmmss please");
      return;
    }

    int hour = timeStr.substring(0,2).toInt();
    int minute = timeStr.substring(2,4).toInt();
    int seconds = timeStr.substring(4).toInt();

    RtcDateTime now = Rtc.GetDateTime();
    RtcDateTime newTime = RtcDateTime(now.Year(), now.Month(), now.Day(), hour, minute, seconds);
    Rtc.SetDateTime(newTime);
  }
}

void scmd_getTemp() {
  RtcTemperature temp = Rtc.GetTemperature();
  temp.Print(Serial);
  Serial.println("C");
}